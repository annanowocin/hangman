(function () {

  var currentWord = '';
  var wrongLetters = '';
  var correctLetters = '';

  function getElement(id) {
    return document.getElementById(id);
  }

  function showElement(id) {
    getElement(id).style.display = 'block';
  }

  function hideElement(id) {
    getElement(id).style.display = 'none';
  }

  function resetGame() {
    wrongLetters = '';
    correctLetters = '';
    hideElement('js-game-over');
    hideElement('js-you-win');
    getElement('js-missed-letters').innerHTML = '';
    for (var i = 1; i <= 11; i++) {
      hideElement('js-hangman-' + i);
      getElement('js-letter-' + i).innerHTML = '';
      getElement('js-letter-' + i).classList.remove('active');
    }
  }

  function newGame() {
    fetch("https://words.desknet.pl/basic/5/11").then(function (resp) {
      return resp.ok ? resp.json() : Promise.reject(resp);
    }).then(function (resp) {
      resetGame();
      currentWord = resp.name;
      for (var i = 12 - currentWord.length; i <= 11; i++) {
        getElement('js-letter-' + i).classList.add('active');
      }
    }).catch(function (error) {
      alert('Wystąpił błąd');
    });
  }

  function addCorrectLetter(key) {
    if (correctLetters.indexOf(key) !== -1) {
      return;
    }
    for (var i = 0; i < currentWord.length; i++) {
      if (currentWord[i] === key) {
        correctLetters += key;
        getElement('js-letter-' + (12 - currentWord.length + i)).innerHTML = key.toUpperCase();
        if (currentWord.length === correctLetters.length) {
          showElement('js-you-win');
        }
      }
    }
  }

  function addIncorrectLetter(key) {
    if (wrongLetters.indexOf(key) !== -1) {
      return;
    }
    wrongLetters += key;
    showElement('js-hangman-' + wrongLetters.length);
    getElement('js-missed-letters').innerHTML += '<span>' + key.toUpperCase() + '</span>';
    if (wrongLetters.length > 10) {
      showElement('js-game-over');
    }
  }

  function isCorrectKeyAndGameInProgress(key) {
    return key.length === 1 && wrongLetters.length < 11 && correctLetters.length < currentWord.length;
  }

  function isCharacterInWord(key) {
    return currentWord.indexOf(key) !== -1;
  }

  getElement('js-new-word-1').addEventListener('click', function () { newGame(); });
  getElement('js-new-word-2').addEventListener('click', function () { newGame(); });

  document.addEventListener('keyup', function (ev) {
    if (isCorrectKeyAndGameInProgress(ev.key)) {
      isCharacterInWord(ev.key) ? addCorrectLetter(ev.key) : addIncorrectLetter(ev.key);
    }
  });

})();
